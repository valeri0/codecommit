resource "aws_codecommit_repository" "single" {
  for_each        = var.name
  repository_name = each.key
  tags            = var.use_always_additional_tags == true && !contains(var.name_without_additional_tags, each.key) ? local.all_tags : contains(var.name_with_additional_tags, each.key) ? local.all_tags : var.tags
}

resource "aws_codecommit_repository" "deploy" {
  for_each = {
    for key, value in var.name :
    key => key
    if value == "deploy"
  }
  repository_name = "${each.key}-deploy"
  tags            = var.use_always_additional_tags == true && !contains(var.name_without_additional_tags, each.key) ? local.all_tags : contains(var.name_with_additional_tags, each.key) ? local.all_tags : var.tags
}

