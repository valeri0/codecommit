terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.65.0"
    }
  }
  required_version = ">= 0.15.0, < 2.0.0"
}
