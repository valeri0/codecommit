## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| additional\_tags | map of names of the repository inferred by directory name | `map(string)` | n/a | yes |
| enabled | since terraform 0.12 does not supports conditional modules this is an hack to use them | `string` | `"true"` | no |
| name | repository name | `map` | n/a | yes |
| name\_with\_additional\_tags | list of names with additional tag | `list` | n/a | yes |
| name\_without\_additional\_tags | list of names without additional tag | `list` | `[]` | no |
| tags | returning by default label as requested by the architect | `map` | <pre>{<br>  "Project": "FactoryDataHub"<br>}</pre> | no |
| use\_always\_additional\_tags | if set to true name\_with\_additional\_tags is like contains '\*' | `bool` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| repository\_name | returns repository name |
