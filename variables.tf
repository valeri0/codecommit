locals {
  all_tags = merge(var.tags, var.additional_tags)
}
variable "additional_tags" {
  description = "map of names of the repository inferred by directory name"
  type        = map(string)
}
variable "name" {
  description = "repository name"
  type        = map(any)
}
variable "name_with_additional_tags" {
  description = "list of names with additional tag"
  type        = list(any)
}
variable "name_without_additional_tags" {
  default     = []
  description = "list of names without additional tag"
  type        = list(any)
}
variable "tags" {
  default = {
    Project = "FactoryDataHub"
  }
  description = "returning by default label as requested by the architect"
  type        = map(any)
}

variable "use_always_additional_tags" {
  description = "if set to true name_with_additional_tags is like contains '*'"
  type        = bool
}
